﻿///基于unity5.4.4的SpriteDefault
Shader "Sprites/RectFill"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		[MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
		///1:横向；2:纵向；
		_FillMethod ("FillMethod", Int) = 1
		//1Method(1:从左到右显示；2:从右到左显示)
		//2Method(1:从下到上显示；2:从上到下显示)
		_FillOrigin ("FillOrigin", Int) = 1
		_FillAmount ("FillAmount", Range(1, 3)) = 0.7
		_AlphaAmount ("AlphaAmount", Range(0,1)) = 0
		_FadingPercent("FadingPercent", Range(0,1)) = 0
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Blend One OneMinusSrcAlpha

		Pass
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0
			#pragma multi_compile _ PIXELSNAP_ON
			#pragma multi_compile _ ETC1_EXTERNAL_ALPHA
			#include "UnityCG.cginc"
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				float2 texcoord  : TEXCOORD0;
			};
			
			fixed4 _Color;
			int _FillMethod;
			int _FillOrigin;
			fixed _FillAmount;
			fixed _FadingPercent;

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				OUT.vertex = UnityObjectToClipPos(IN.vertex);
				OUT.texcoord = IN.texcoord;
				OUT.color = IN.color * _Color;
				#ifdef PIXELSNAP_ON
				OUT.vertex = UnityPixelSnap (OUT.vertex);
				#endif

				return OUT;
			}

			sampler2D _MainTex;
			sampler2D _AlphaTex;

			fixed4 SampleSpriteTexture (float2 uv)
			{
				fixed4 color = tex2D (_MainTex, uv);

                /*
                if ( uv.x > 0.02 && uv.x < 0.98 && uv.y > 0.05 && uv.y < 0.95 )
                {
                   discard;
                }
                else
                {
                  */
             
                fixed dis = ( uv.x - 0.5 ) * ( uv.x - 0.5 ) + ( uv.y - 0.5 ) * ( uv.y - 0.5 );
                fixed fAlpha = _FillAmount * (dis * dis / 0.5);
                color.a *= fAlpha;
			
                /*
				if(1 == _FillMethod)
				{
					if(1 == _FillOrigin && uv.x>_FillAmount)
					{
						discard;
					}
					else if(2 == _FillOrigin && uv.x<(1-_FillAmount))
					{
						discard;
					}
				}
				else if(2 == _FillMethod)
				{
					if(1 == _FillOrigin && uv.y>_FillAmount)
					{
						discard;
					}
					else if(2 == _FillOrigin && uv.y<(1-_FillAmount))
					{
						discard;
					}
				}

				if ( _FadingPercent > 0 )
				{
					fixed fAlpha = 1;
					if (_FillOrigin == 1)
					{
						fAlpha = uv.x - _FadingPercent;
					}
					else
					{
						fAlpha = _FadingPercent - uv.x;
					}

					if ( fAlpha < 0 )
					{
						fAlpha = 0;
					}
					color.a *= fAlpha;
				}
                */

				return color;
			}

			fixed4 frag(v2f IN) : SV_Target
			{
				fixed4 c = SampleSpriteTexture (IN.texcoord) * IN.color;
				c.rgb *= c.a;
				return c;
			}
		ENDCG
		}
	}

}
