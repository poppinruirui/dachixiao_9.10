﻿using UnityEngine;
using System.Collections;
 
namespace MagicArsenal
{
    public class MagicRotation : MonoBehaviour
    {
        static Vector3 vecTempRotate = new Vector3();
 
        [Header("Rotate axises by degrees per second")]
        public Vector3 rotateVector = Vector3.zero;
 
        public enum spaceEnum { Local, World };
        public spaceEnum rotateSpace;
 
        // Use this for initialization
        void Start()
        {
 
        }
 
        // Update is called once per frame
        void Update()
        {

            if (rotateSpace == spaceEnum.Local)
            {
                vecTempRotate = rotateVector * Time.deltaTime;
                vecTempRotate.x = 0;
                vecTempRotate.y = 0;
                transform.Rotate(vecTempRotate);
               // transform.Rotate(rotateVector * Time.deltaTime);
                this.transform.localRotation = Quaternion.identity;
                this.transform.Rotate(0.0f, 0.0f, Time.deltaTime);
            }
            if (rotateSpace == spaceEnum.World)
            {
                transform.Rotate(rotateVector * Time.deltaTime, Space.World);
            }
        }
    }
}