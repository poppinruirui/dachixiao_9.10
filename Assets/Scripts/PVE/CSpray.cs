﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CSpray : CPveObj {

    static Vector3 vecTempPos = new Vector3();

	CSpryEditor.sSprayConfig m_Config;

	public GameObject m_goMonsterContainer;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		DoSpray ();
		LifeTimeLoop ();
	}

	public void SetConfig( CSpryEditor.sSprayConfig config )
	{
		m_Config = config;
	}

	public CSpryEditor.sSprayConfig GetConfig(  )
	{
		return m_Config;
	}

	float m_fSprayTimeCount = 0f;
	void DoSpray()
	{
		m_fSprayTimeCount += Time.deltaTime;
		if (m_fSprayTimeCount < m_Config.fInterval) {
			return;
		}
		m_fSprayTimeCount = 0f;
		int nMonsterNum = (int)m_Config.fDensity;
		List<Vector2> lstDir = MapEditor.s_Instance.GetExplodeDirList (nMonsterNum);
		float fDistance = (float)UnityEngine.Random.Range ( m_Config.fMinDis, m_Config.fMaxDis );
		List<CMonster> lstMonsters = new List<CMonster> ();
		for (int i = 0; i < nMonsterNum; i++) {
			CMonster monster = ResourceManager.s_Instance.ReuseMonster ();
			monster.SetConfigId ( m_Config.szThornId );
			monster.SetMonsterType (CMonster.eMonsterType.spray);
            //monster.transform.parent = m_goMonsterContainer.transform;
            vecTempPos = GetPos();
            vecTempPos.z = 500f;// -monster.GetSize();   // poppin test
            monster.SetPos ( GetPos() );
			monster.SetDir ( lstDir[i] );
			monster.BeginEject ( fDistance, 1.0f );
			lstMonsters.Add (monster);
		}
		m_lstLifeTimeLoop.Add ( lstMonsters );
		m_lstLifeTimeLoop_SprayTime.Add ((float)Time.time);
	}

	List<float> m_lstLifeTimeLoop_SprayTime = new List<float>();
	List<List<CMonster>> m_lstLifeTimeLoop = new List<List<CMonster>>();
	void LifeTimeLoop()
	{
		if (m_lstLifeTimeLoop_SprayTime.Count == 0) {
			return;
		}

		if (Time.time - m_lstLifeTimeLoop_SprayTime [0] >= m_Config.fLifeTime) {
			List<CMonster> lstToRecycle = m_lstLifeTimeLoop [0];
			for (int i = 0; i < lstToRecycle.Count; i++) {
				ResourceManager.RecycleMonster ( lstToRecycle[i] );
			}
			m_lstLifeTimeLoop.RemoveAt (0);
			m_lstLifeTimeLoop_SprayTime.RemoveAt (0);
		}
	}

	public void ProcessOnStay( Ball ball )
	{
		float fCurSize = ball.GetSize ();
		fCurSize -= fCurSize * 0.02f;
		ball.Local_SetSize ( fCurSize );
		if (ball._Player && ball._Player.IsMainPlayer ()) {
			if (fCurSize < Main.BALL_MIN_SIZE) {
				ball._Player.DestroyBall ( ball.GetIndex() );
			}
		}
	}
}
