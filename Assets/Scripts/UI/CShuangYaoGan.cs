﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CShuangYaoGan : MonoBehaviour {

    static Vector3 vecTempPos = new Vector3();

    public Image _imgKnob;

    public float m_fRadius = 320;

    bool m_bShowing = false;
    Vector2 m_vecDir = new Vector2();

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        UpdateKnobPos();

    }

    public void SetVisible( bool bVisible )
    {
        this.gameObject.SetActive( bVisible );
        m_bShowing = bVisible;
    }

    public void UpdateKnobPos()
    {
        if ( !m_bShowing)
        {
            return;
        }

        m_vecDir = Main.s_Instance.GetSpitDirection();
        vecTempPos = _imgKnob.transform.localPosition;
        vecTempPos.x = m_fRadius * m_vecDir.x;
        vecTempPos.y = m_fRadius * m_vecDir.y;
        _imgKnob.transform.localPosition = vecTempPos;

    }
}
