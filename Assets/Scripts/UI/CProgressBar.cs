﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CProgressBar : MonoBehaviour {

    public Text _txtCaption;
    public Image _imgBar;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetCaption( string szCaption )
    {
        _txtCaption.text = szCaption;
    }

    public void SetfillAmount( float fFillAmount )
    {
        _imgBar.fillAmount = fFillAmount;
    }

    public float GetValue()
    {
        return _imgBar.fillAmount;
    }
}
