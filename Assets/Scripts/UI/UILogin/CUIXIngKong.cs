﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CUIXIngKong : MonoBehaviour {

    public float m_fMainCameraMoveSpeed = 1f;

    static Vector3 vecTempPos = new Vector3();

    public Camera cameraMain;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

     

	}

    private void FixedUpdate()
    {
        vecTempPos = cameraMain.transform.position;
        vecTempPos.x -= m_fMainCameraMoveSpeed * Time.fixedDeltaTime;
        cameraMain.transform.position = vecTempPos;
    }
}
