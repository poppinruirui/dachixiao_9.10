﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CTriggerForEatBean : MonoBehaviour {

	public Ball _ball;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// 接触
	void OnTriggerEnter2D(Collider2D other)
	{
		//other.gameObject.SetActive ( false );
		CBean bean = other.gameObject.GetComponent<CBean>();
		this._ball.EatBean_New(bean); 
		//this._ball.SetEatBeanTriggerEnable ( false );
	}
}
