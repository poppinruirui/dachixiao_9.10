﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class CSkeletonAnimation : CCosmosEffect  {

	public SkeletonAnimation _skeletonMain;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void PlayAnimation(  int nTrack, string szAniName, float fTimeScale, bool bLoop )
	{
		// ani04    1
		//_skeletonMain.state.SetAnimation(0, "ani04_1", true);
		//_skeletonMain.state.TimeScale = 0.2f;
		_skeletonMain.state.SetAnimation( nTrack, szAniName, bLoop);
		_skeletonMain.state.TimeScale = fTimeScale;
	}
}
