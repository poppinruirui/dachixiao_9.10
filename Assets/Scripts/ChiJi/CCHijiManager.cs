﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CCHijiManager : MonoBehaviour {

    public bool m_bGameStarted = false;

    public int m_nPlayerNumToStart = 2;

    public GameObject _panelWaiting; //

    public Text _txtPlayerNum;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        MainLoop();


	}

    void MainLoop()
    {
        Waiting_Loop();
        Fighting_Loop();

    }

    void Waiting_Loop()
    {
        if ( m_bGameStarted )
        {
            return;
        }

        if ( PhotonNetwork.room.playerCount >= m_nPlayerNumToStart )
        {
            _panelWaiting.SetActive(false);
            m_bGameStarted = true;
        }
        else
        {
            _txtPlayerNum.text = PhotonNetwork.room.playerCount + "/" + m_nPlayerNumToStart;
            _panelWaiting.SetActive( true );
        }


    }

    void Fighting_Loop()
    {
        if (!m_bGameStarted)
        {
            return;
        }



    }

   
}
