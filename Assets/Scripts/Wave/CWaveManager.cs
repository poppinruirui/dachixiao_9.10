﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CWaveManager : MonoBehaviour {

    public static CWaveManager s_Instance = null;

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();


    public GameObject m_preWave;
    public GameObject m_preWaveGroup;
    public GameObject m_preWaveGrid;

    public float m_fDeltaXInGroup = 1.8221f;
    public float m_fDeltaYInGroup = 1.0639f;

    public float m_fGroupInitDeltaX = 0;
    public float m_fGroupInitDeltaY = 0;

    public float m_fCurScale = 3f;

    public float m_fSizeXiShu = 1;
    public float m_fIntervalXiShu = 1;

    float[] m_aryThreshold = new float[80];
    float[] m_aryThresholdValue = new float[80];
    float m_fLastCamSize = -1f;

    public float m_fGridWidth = 0;
    public float m_fGridHeight = 0;
    public float m_fGridOffsetX = 0;

    private void Awake()
    {
        s_Instance = this;
    }

    Dictionary<int, CWaveGroup> m_dicGroups = new Dictionary<int, CWaveGroup>();


    // Use this for initialization  过早优化，万恶之源。 
    void Start () {
        return;

        InitThreshold();

        for (int j = -80; j < 80; j++) // 生成group
        {
            CWaveGroup group = NewWaveGroup();
            vecTempPos.x = j * m_fGroupInitDeltaX * m_fCurScale;
            vecTempPos.y = 0;
            vecTempPos.z = 0;
            group.transform.SetParent(this.transform);
            group.SetPos(vecTempPos);
            group.SetScale(m_fCurScale);

            m_dicGroups[j] = group;

            for (int i = -80; i < 80; i++) // 生成wave
            {
                CWave wave = NewWave();
                vecTempPos.x = m_fDeltaXInGroup * i;
                vecTempPos.y = m_fDeltaYInGroup * i;
                vecTempPos.z = 0f;
                group.AddWave(wave);
                wave.SetPos(vecTempPos);
                wave.SetScale(1f);
                wave.SetIndex( i );
            } // end i
        } // end j

        SetSize( m_fSizeXiShu );
        SetInterval( m_fIntervalXiShu );


        UpdateWaveByCameraSize();


    }

    void InitThreshold()
    {
        for ( int i = 0; i < 40; i++ )
        {
            m_aryThreshold[i] = 10 * ( i + 1 );
            m_aryThresholdValue[i] = 0.233f * m_aryThreshold[i];
        }
    }

    // Update is called once per frame
    void Update()
    {
        return;

        UpdateWaveByCameraSize();
        ChangingLoop_Out();
        ChangingLoop_In();
    }

    List<CWave> m_lstRecycledWave = new List<CWave>();
    public CWave NewWave()
    {
        CWave wave = null;
        if (m_lstRecycledWave.Count > 0)
        {
            wave = m_lstRecycledWave[0];
            wave.gameObject.SetActive( true );
            m_lstRecycledWave.RemoveAt(0);
        }
        else
        {
            wave = GameObject.Instantiate(m_preWave).GetComponent<CWave>();
        }
        return wave;
    }

    public void DeleteWave( CWave wave )
    {
        wave.gameObject.SetActive(false);
        m_lstRecycledWave.Add( wave );
    }

    public CWaveGroup NewWaveGroup()
    {
        return GameObject.Instantiate(m_preWaveGroup).GetComponent<CWaveGroup>();
    }

    public CMyWaveGrid NewWaveGrid()
    {
        return GameObject.Instantiate(m_preWaveGrid).GetComponent<CMyWaveGrid>();
    }

    public float GetSize()
    {
        return m_fCurScale;
    }


    public void SetSize( float fSize )
    {
        m_fCurScale = fSize;

        foreach( KeyValuePair<int, CWaveGroup> pair in m_dicGroups )
        {
            pair.Value.SetScale(m_fCurScale);
        }

        SetInterval( GetInterval() );
    }
   
    public void SetInterval( float fInterval )
    {
        m_fGroupInitDeltaX = fInterval;
        foreach (KeyValuePair<int, CWaveGroup> pair in m_dicGroups)
        {
            vecTempPos.x = pair.Key * m_fGroupInitDeltaX * m_fCurScale;
            vecTempPos.y = 0;
            vecTempPos.z = 0;
            pair.Value.SetPos(vecTempPos);
        }
    }

    public float GetInterval()
    {
        return m_fGroupInitDeltaX;
    }

    void UpdateWaveByCameraSize( bool bDirect = false )
    {
        if ( CCameraManager.s_Instance == null )
        {
            return;
        }
 
        float fCamSize = CCameraManager.s_Instance.GetCurCameraSize();
        for ( int i = m_aryThreshold.Length - 1; i >= 1; i--  )
        {
            if ( m_fLastCamSize < m_aryThreshold[i] && fCamSize >= m_aryThreshold[i] ) 
            {
                BeginChangeLevel(i);
            }

            if (m_fLastCamSize >= m_aryThreshold[i] && fCamSize < m_aryThreshold[i])
            {
                BeginChangeLevel(i - 1);
            }
        }
        m_fLastCamSize = fCamSize;
    }

    float m_fCurAlpha = 1f;
    bool m_bChangingOut = false;
    bool m_bChangingIn = false;
    int m_nDestLevel = 0;
    void BeginChangeLevel( int nLevel )
    {
        if (m_bChangingOut)
        {

        }
        else
        {
            m_fCurAlpha = 1f;
        }
        m_bChangingOut = true;
        m_nDestLevel = nLevel;
    }

    void ChangingLoop_Out()
    {
        if ( !m_bChangingOut)
        {
            return;
        }

        foreach (KeyValuePair<int, CWaveGroup> pair in m_dicGroups)
        {
            pair.Value.SetAlpha(m_fCurAlpha);
        }
        m_fCurAlpha -= Time.deltaTime;
        if(m_fCurAlpha <= 0)
        {
            EndChange_Out();
        }
    }

    void EndChange_Out()
    {
        m_bChangingOut = false;
        m_bChangingIn = true;
        m_fCurAlpha = 0;

        float fThresholdValue = m_aryThresholdValue[m_nDestLevel];
        float fCurScale = fThresholdValue;
        float fInterval = m_fIntervalXiShu;
        SetSize(fCurScale);
        SetInterval(fInterval);

    }

    void ChangingLoop_In()
    {
        if (!m_bChangingIn)
        {
            return;
        }

        foreach (KeyValuePair<int, CWaveGroup> pair in m_dicGroups)
        {
            pair.Value.SetAlpha(m_fCurAlpha);
        }
        m_fCurAlpha += Time.deltaTime;
        if (m_fCurAlpha >= 1)
        {
            EndChange_In();
        }
    }

    void EndChange_In()
    {
        m_bChangingIn = false;
    }
}
