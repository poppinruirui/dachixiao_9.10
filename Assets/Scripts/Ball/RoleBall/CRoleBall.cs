﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CRoleBall : MonoBehaviour {

    public SpriteRenderer _srYanBai; // 眼白

    public SpriteRenderer[] m_aryHair;
    public SpriteRenderer[] m_aryHat;
    public GameObject _containerHat;

    public static Color colorTmp;

    public Vector2 m_vecEyeMaskPos_Common;
    public Vector2 m_vecEyeMaskPos_Skill;

    public Ball _ball;

    CRoleManager.eRoleFaceType m_eCurStatus = CRoleManager.eRoleFaceType.common;

    public SpriteMask _smMaskEye; // 眼眶遮罩
    public GameObject _containerEyeBling;    // 眼睛闪烁

    public SpriteRenderer _srEyeAffected; // 被影响时的眼睛
    public SpriteRenderer _srEyeSkill; // 技能时的眼睛

    public GameObject _containerRole;
    public GameObject _containerSimple;

    public GameObject _containerEar0;
    public GameObject _containerEar1;
    public SpriteRenderer _goEar0;    
    public SpriteRenderer _goEar1;
    public SpriteRenderer _goHat;

    public SpriteRenderer _srQingJin;

    public SpriteRenderer _srScarf; // 围巾

    static Vector3 vecTempPos = new Vector3();

    public SpriteRenderer _srFace;

    public SpriteRenderer _srNose;

    public SpriteRenderer _srHair;
    public SpriteRenderer _srHair1;
    public SpriteRenderer _srHair2;

    public GameObject _containerEye0;
    public GameObject _containerEye1;
    public SpriteRenderer _srEye0;
    public SpriteRenderer _srEye1;
    public SpriteRenderer _srSunGlasses;


   


    int m_nHairLoopStatus = 0;
    float m_fHairAngle = 0;

    int m_nHair2LoopStatus = 0;
    float m_fHair2Angle = 0;

    int m_nSunGlassesStatus = 0;
    float m_fSunGlassesMoveAmount = 0;

    public void Clear()
    {
        
    }

	// Use this for initialization
	void Start () {


	}

    public void SetAlpha( float fAlpha )
    {
        if ( _srEyeSkill )
        {
            colorTmp = _srEyeSkill.color;
            colorTmp.a = fAlpha;
            _srEyeSkill.color = colorTmp;
        }

        if (_srEyeAffected)
        {
            colorTmp = _srEyeAffected.color;
            colorTmp.a = fAlpha;
            _srEyeAffected.color = colorTmp;
        }

        if ( _srFace )
        {
            colorTmp = _srFace.color;
            colorTmp.a = fAlpha;
            _srFace.color = colorTmp;
        }

        if (_srEye0 )
        {
            colorTmp = _srEye0.color;
            colorTmp.a = fAlpha;
            _srEye0.color = colorTmp;
        }

        if (_srEye1)
        {
            colorTmp = _srEye1.color;
            colorTmp.a = fAlpha;
            _srEye1.color = colorTmp;
        }


        if (_srNose)
        {
            colorTmp = _srNose.color;
            colorTmp.a = fAlpha;
            _srNose.color = colorTmp;
        }


        if (_srScarf)
        {
            colorTmp = _srScarf.color;
            colorTmp.a = fAlpha;
            _srScarf.color = colorTmp;
        }

        if (_srHair)
        {
            colorTmp = _srHair.color;
            colorTmp.a = fAlpha;
            _srHair.color = colorTmp;
        }

        if (_srHair1)
        {
            colorTmp = _srHair1.color;
            colorTmp.a = fAlpha;
            _srHair1.color = colorTmp;
        }

        if (_srHair2)
        {
            colorTmp = _srHair2.color;
            colorTmp.a = fAlpha;
            _srHair2.color = colorTmp;
        }

        if (_srSunGlasses)
        {
            colorTmp = _srSunGlasses.color;
            colorTmp.a = fAlpha;
            _srSunGlasses.color = colorTmp;
        }

        if (_goEar0)
        {
            colorTmp = _goEar0.color;
            colorTmp.a = fAlpha;
            _goEar0.color = colorTmp;
        }

        if (_goEar1)
        {
            colorTmp = _goEar1.color;
            colorTmp.a = fAlpha;
            _goEar1.color = colorTmp;
        }

        if (_goHat)
        {
            colorTmp = _goHat.color;
            colorTmp.a = fAlpha;
            _goHat.color = colorTmp;
        }

        for (int i = 0; i < m_aryHair.Length; i++ )
        {
            SpriteRenderer srHair = m_aryHair[i];
            if ( srHair == null )
            {
                continue;
            }
            colorTmp = srHair.color;
            colorTmp.a = fAlpha;
            srHair.color = colorTmp;
        }

        for (int i = 0; i < m_aryHat.Length; i++)
        {
            SpriteRenderer srHat = m_aryHat[i];
            if (srHat == null)
            {
                continue;
            }
            colorTmp = srHat.color;
            colorTmp.a = fAlpha;
            srHat.color = colorTmp;
        }

    }

    public void Show( bool bShow )
    {
        _containerRole.SetActive( bShow );
        _containerSimple.SetActive(!bShow);
    }
	
   

	// Update is called once per frame
	void Update () {

        EyeLoop();
        HairLoop();
        SunGlassesLoop();
        FaceAffectedLoop();
        HatContainerLoop();
	}

    private void FixedUpdate()
    {
        EarLoop();

    }

    public void SetColor( Color color )
    {

       // _effect.SetColor( color );
    }

    void EyeLoop()
    {
        if ( !_ball._Player.IsMainPlayer() )
        {
            return;
        }

        float fRadius = 0.08f;
        vecTempPos.x = PlayerAction.s_Instance._joystick_movement.x * fRadius;
        vecTempPos.y = PlayerAction.s_Instance._joystick_movement.y * fRadius;
        _srEye0.transform.localPosition = vecTempPos;
        _srEye1.transform.localPosition = vecTempPos;
    
    }

    int m_nHatContainerStatus = 0;
    float m_fHatContainerMoveAmount = 0;
    void HatContainerLoop()
    {
        if (_containerHat == null)
        {
            return;
        }

        float fMaxMoveAmount = 0.03f;
        float fMoveSpeed = 0.03f;
        if (m_nHatContainerStatus == 0)
        {
            vecTempPos = _containerHat.transform.localPosition;
            float fMoveAmount = Time.deltaTime * fMoveSpeed;
            vecTempPos.y += fMoveAmount;
            m_fHatContainerMoveAmount += fMoveAmount;
            if (m_fHatContainerMoveAmount > fMaxMoveAmount)
            {
                m_nHatContainerStatus = 1;
            }
            _containerHat.transform.localPosition = vecTempPos;
        }
        else
        {
            vecTempPos = _containerHat.transform.localPosition;
            float fMoveAmount = Time.deltaTime * fMoveSpeed;
            vecTempPos.y -= fMoveAmount;
            m_fHatContainerMoveAmount -= fMoveAmount;
            if (m_fHatContainerMoveAmount < -fMaxMoveAmount)
            {
                m_nHatContainerStatus = 0;
            }
            _containerHat.transform.localPosition = vecTempPos;
        }


    }


    void SunGlassesLoop()
    {
        if ( _srSunGlasses == null )
        {
            return;
        }

        float fMaxMoveAmount = 0.03f;
        float fMoveSpeed = 0.03f;
        if ( m_nSunGlassesStatus == 0 )
        {
            vecTempPos = _srSunGlasses.transform.localPosition;
            float fMoveAmount = Time.deltaTime * fMoveSpeed;
            vecTempPos.y += fMoveAmount;
            m_fSunGlassesMoveAmount += fMoveAmount;
            if ( m_fSunGlassesMoveAmount > fMaxMoveAmount )
            {
                m_nSunGlassesStatus = 1;
            } 
            _srSunGlasses.transform.localPosition = vecTempPos;
        }
        else
        {
            vecTempPos = _srSunGlasses.transform.localPosition;
            float fMoveAmount = Time.deltaTime * fMoveSpeed;
            vecTempPos.y -= fMoveAmount;
            m_fSunGlassesMoveAmount -= fMoveAmount;
            if (m_fSunGlassesMoveAmount < -fMaxMoveAmount)
            {
                m_nSunGlassesStatus = 0;
            }
            _srSunGlasses.transform.localPosition = vecTempPos;
        }


    }

    public void Init()
    {
        switch( _ball.GetHeroType() )
        {
            case CRoleManager.eHeroType.yanmie:
                {
                    Init_YanMie();
                }
                break;
            case CRoleManager.eHeroType.guiwan:
                {
                    Init_GuiWan();
                }
                break;
        } // end switch
    }

    public void Init_GuiWan()
    {
        m_aryHairSpeed[0] = 2f * Time.fixedDeltaTime;
        m_aryHairSpeed[1] = 0.05f * Time.fixedDeltaTime;
        m_aryHairSpeed[2] = 0.4f * Time.fixedDeltaTime; 
    }

    public void Init_YanMie()
    {
        m_aryHairSpeed[0] = 0.2f * Time.fixedDeltaTime;
        m_aryHairSpeed[1] = 0.05f * Time.fixedDeltaTime;
        m_aryHairSpeed[2] = 0.4f * Time.fixedDeltaTime; 
    }

    float[] m_aryHairSpeed = new float[4];
    float[] m_aryHairAngle = new float[4];
    void YanMie_Hair_Loop()
    {
        for (int i = 0; i < 3; i++ )
        {
            SpriteRenderer srHair = m_aryHair[i];
            if (m_nHairLoopStatus == 0)
            {
                m_fHairAngle -= m_aryHairSpeed[i];
                if (m_fHairAngle < -2.5f)
                {
                    m_nHairLoopStatus = 1;
                }
            }
            else
            {
                m_fHairAngle += m_aryHairSpeed[i];
                if (m_fHairAngle > 2.5f)
                {
                    m_nHairLoopStatus = 0;
                }
            }
            srHair.transform.localRotation = Quaternion.identity;
            srHair.transform.Rotate(0.0f, 0.0f, m_fHairAngle);

        }
    }

    void GuiWan_Hair_Loop()
    {
        for (int i = 0; i < 1; i++)
        {
            SpriteRenderer srHair = m_aryHair[i];
            if (m_nHairLoopStatus == 0)
            {
                m_fHairAngle -= m_aryHairSpeed[i];
                if (m_fHairAngle < -5f)
                {
                    m_nHairLoopStatus = 1;
                }
            }
            else
            {
                m_fHairAngle += m_aryHairSpeed[i];
                if (m_fHairAngle > 5f)
                {
                    m_nHairLoopStatus = 0;
                }
            }
           
            srHair.transform.localRotation = Quaternion.identity;
            srHair.transform.Rotate(0.0f, 0.0f, m_fHairAngle);
        }
    }

    void HairLoop()
    {
        if (_ball.GetHeroType() == CRoleManager.eHeroType.yanmie)
        {
            YanMie_Hair_Loop();
        }
        else if (_ball.GetHeroType() == CRoleManager.eHeroType.guiwan)
        {
            GuiWan_Hair_Loop();
        }

        if (_srHair)
        {
            if (m_nHairLoopStatus == 0)
            {
                m_fHairAngle -= Time.deltaTime * 5f;
                if (m_fHairAngle < -5)
                {
                    m_nHairLoopStatus = 1;
                }
            }
            else
            {
                m_fHairAngle += Time.deltaTime * 5f;
                if (m_fHairAngle > 0)
                {
                    m_nHairLoopStatus = 0;
                }
            }
            _srHair.transform.localRotation = Quaternion.identity;
            _srHair.transform.Rotate(0.0f, 0.0f, m_fHairAngle);

        }

        if (_srHair1 || _srHair2)
        {
            if (m_nHair2LoopStatus == 0)
            {
                m_fHair2Angle += Time.deltaTime * 2f;
                if (m_fHair2Angle >= 5)
                {
                    m_nHair2LoopStatus = 1;
                }
            }
            else
            {
                m_fHair2Angle -= Time.deltaTime * 2f;
                if (m_fHair2Angle <= 0)
                {
                    m_nHair2LoopStatus = 0;
                }
            }
            if (_srHair1)
            {
                _srHair1.transform.localRotation = Quaternion.identity;
                _srHair1.transform.Rotate(0.0f, 0.0f, m_fHair2Angle);
            }

            if (_srHair2)
            {
                _srHair2.transform.localRotation = Quaternion.identity;
                _srHair2.transform.Rotate(0.0f, 0.0f, m_fHair2Angle);
            }
        }

    }



    //   _srNose.transform.localRotation = Quaternion.identity;
    //   _srNose.transform.Rotate(0.0f, 0.0f, m_fNoseAngle);     

    public void BeginFaceAffected()
    {
         m_fFaceAffectedTimeElapse = 0;
        m_bAffected = true;
    }

    public void EndFaceAffected()
    {
        ChangeFace(  CRoleManager.eRoleFaceType.common);
    }

    float m_fFaceAffectedTimeElapse = 0;
    bool m_bAffected = false;
    void FaceAffectedLoop()
    {
        if ( m_eCurStatus != CRoleManager.eRoleFaceType.being_affected )
        {
            return;
        }
        m_fFaceAffectedTimeElapse += Time.deltaTime;
        if ( m_fFaceAffectedTimeElapse >= 1 )
        {
            EndFaceAffected();
        }
    }

    public CRoleManager.eHeroType GetHeroType()
    {
        return _ball.GetHeroType();
    }

    public void ChangeFace(CRoleManager.eRoleFaceType type )
    {
        m_eCurStatus = type;
        m_nEarActionStatus = 1;
        m_fEarLoopTimeElapse = 0;

        if (_ball._Player.GetHeroType() == CRoleManager.eHeroType.luoman)
        {

        }
        else
        {
            _srFace.sprite = CRoleManager.s_Instance.GetFaceSpriteByType(_ball._Player.GetHeroType(), type);
        }

        if ( type == CRoleManager.eRoleFaceType.being_affected )
        {
       
            if (_ball.GetHeroType() == CRoleManager.eHeroType.yanmie)
            {
                _smMaskEye.gameObject.SetActive(false);
                _containerEyeBling.SetActive(false);

                _srQingJin.gameObject.SetActive( true );
            }
            else if (_ball.GetHeroType() == CRoleManager.eHeroType.guiwan)
            {
                    _smMaskEye.sprite = CRoleManager.s_Instance.GetEyeMaskSprite(GetHeroType(), CRoleManager.eRoleFaceType.being_affected);
                    _smMaskEye.transform.localPosition = CRoleManager.s_Instance.m_aryEyeMaskPos_Affected[(int)GetHeroType()];
                    _smMaskEye.gameObject.SetActive(true);

                    _containerEye0.transform.localPosition = CRoleManager.s_Instance.m_aryEye0ContainerLocalPos_Affected[(int)_ball.GetHeroType()];
                    _containerEye1.SetActive(false);

            }
            else if (_ball.GetHeroType() == CRoleManager.eHeroType.luoman)
            {
                _srYanBai.color = Color.white;
                _smMaskEye.gameObject.SetActive(false);
            }


            BeginFaceAffected();
            if ( _ball.GetHeroType() == CRoleManager.eHeroType.guiwan )
            {
                _smMaskEye.gameObject.SetActive(true);
            }
            else
            {
                _smMaskEye.gameObject.SetActive(false); 
            }
                           
          
            if (_srEyeAffected)
            {
                _srEyeAffected.gameObject.SetActive(true);
            }
            if (_srEyeSkill)
            {
                _srEyeSkill.gameObject.SetActive(false);
            }
            if ( _srNose )
            {
                _srNose.transform.localPosition = CRoleManager.s_Instance.m_vecNosePos_Affected[(int)_ball._Player.GetHeroType()];
            }

           

        }
        else if ( type == CRoleManager.eRoleFaceType.common )
        {
            if (_ball.GetHeroType() == CRoleManager.eHeroType.yanmie)
            {
                _containerEyeBling.SetActive(false);
                _smMaskEye.gameObject.SetActive(true);
                _srQingJin.gameObject.SetActive(false);
            }
            else if (_ball.GetHeroType() == CRoleManager.eHeroType.guiwan)
            {
                _containerEye0.transform.localPosition = CRoleManager.s_Instance.m_aryEye0ContainerLocalPos_Common[(int)_ball.GetHeroType()];
                _containerEye1.SetActive( true );
            }
            else if (_ball.GetHeroType() == CRoleManager.eHeroType.luoman)
            {
                _srYanBai.color = Color.white;
                _smMaskEye.gameObject.SetActive(true);
            }

            _smMaskEye.sprite = CRoleManager.s_Instance.GetEyeMaskSprite( GetHeroType(), CRoleManager.eRoleFaceType.common );
            _smMaskEye.transform.localPosition = CRoleManager.s_Instance.m_aryEyeMaskPos_Common[(int)GetHeroType()];
            _smMaskEye.gameObject.SetActive(true);
            if (_srEyeAffected)
            {
                _srEyeAffected.gameObject.SetActive(false);
            }
            if (_srEyeSkill)
            {
                _srEyeSkill.gameObject.SetActive(false);
            }
            if (_srNose)
            {
                _srNose.transform.localPosition = CRoleManager.s_Instance.m_vecNosePos_Common[(int)_ball._Player.GetHeroType()];
            }
        }
        else if (  type == CRoleManager.eRoleFaceType.using_skill )
        {
            if (_ball.GetHeroType() == CRoleManager.eHeroType.yanmie)
            {
                _smMaskEye.gameObject.SetActive(false);
                _containerEyeBling.SetActive(true);
                _srQingJin.gameObject.SetActive(false);
            }
            else if (_ball.GetHeroType() == CRoleManager.eHeroType.guiwan)
            {
                _smMaskEye.gameObject.SetActive(false);
                _srEyeSkill.gameObject.SetActive(true);

            }
            else if (_ball.GetHeroType() == CRoleManager.eHeroType.luoman)
            {
                _srYanBai.color = Color.red;
                _smMaskEye.gameObject.SetActive( false );
            }

            _smMaskEye.sprite =  CRoleManager.s_Instance.GetEyeMaskSprite(GetHeroType(), CRoleManager.eRoleFaceType.using_skill);

            if (_ball.GetHeroType() == CRoleManager.eHeroType.luoman)
            {
            }
            else
            {
                _smMaskEye.gameObject.SetActive(true);
            }
            _smMaskEye.transform.localPosition = CRoleManager.s_Instance.m_aryEyeMaskPos_Skill[(int)GetHeroType()];
            if (_srEyeAffected)
            {
                _srEyeAffected.gameObject.SetActive(false);
            }
            if (_srNose)
            {
                _srNose.transform.localPosition = CRoleManager.s_Instance.m_vecNosePos_Skill[(int)_ball._Player.GetHeroType()];
            }
        }

        if (_containerEar0 && _containerEar1)
        {
            switch (type)
            {
                case CRoleManager.eRoleFaceType.common:
                    {

                        _containerEar0.transform.localPosition = CRoleManager.s_Instance.m_vecEar0_CommonPos;
                        _containerEar0.transform.localRotation = Quaternion.identity;
                        _containerEar0.transform.Rotate(0.0f, 0.0f, CRoleManager.s_Instance.m_fEar0_CommonRotation);

                        _containerEar1.transform.localPosition = CRoleManager.s_Instance.m_vecEar1_CommonPos;
                        _containerEar1.transform.localRotation = Quaternion.identity;
                        _containerEar1.transform.Rotate(0.0f, 0.0f, CRoleManager.s_Instance.m_fEar1_CommonRotation);

                    }
                    break;

                case CRoleManager.eRoleFaceType.being_affected:
                    {
                        _containerEar0.transform.localPosition = CRoleManager.s_Instance.m_vecEar0_AffectedPos;
                        _containerEar0.transform.localRotation = Quaternion.identity;
                        _containerEar0.transform.Rotate(0.0f, 0.0f, CRoleManager.s_Instance.m_fEar0_AffectedRotation);

                        _containerEar1.transform.localPosition = CRoleManager.s_Instance.m_vecEar1_AffectedPos;
                        _containerEar1.transform.localRotation = Quaternion.identity;
                        _containerEar1.transform.Rotate(0.0f, 0.0f, CRoleManager.s_Instance.m_fEar1_AffectedRotation);
                    }
                    break;

                case CRoleManager.eRoleFaceType.using_skill:
                    {
                        _containerEar0.transform.localPosition = CRoleManager.s_Instance.m_vecEar0_SkillPos;
                        _containerEar0.transform.localRotation = Quaternion.identity;
                        _containerEar0.transform.Rotate(0.0f, 0.0f, CRoleManager.s_Instance.m_fEar0_SkillRotation);

                        _containerEar1.transform.localPosition = CRoleManager.s_Instance.m_vecEar1_SkillPos;
                        _containerEar1.transform.localRotation = Quaternion.identity;
                        _containerEar1.transform.Rotate(0.0f, 0.0f, CRoleManager.s_Instance.m_fEar1_SkillRotation);
                    }
                    break;

            } // end switch
        } // end  if (_containerEar0 && _containerEar1)
    } //end ChangeFace
       
        float m_fEarLoopTimeElapse = 0;         float m_fEarActionTimeElapse = 0;         int m_nEarActionStatus = 0;          void EarLoop()         {
        if ( _goEar0 == null || _goEar1 == null )
        {
            return;
        }
             if ( m_nEarActionStatus == 0 )             {                 m_fEarLoopTimeElapse += Time.fixedDeltaTime;                 if ( m_fEarLoopTimeElapse >= 5 )                 {                     m_fEarLoopTimeElapse = 0;                     m_nEarActionStatus = 1;                 }             }

        if (m_nEarActionStatus > 0)
        {
            if (m_nEarActionStatus % 2 != 0)
            {
                _goEar0.transform.localRotation = Quaternion.identity;
                _goEar0.transform.Rotate(0.0f, 0.0f,CRoleManager.s_Instance.m_fEarShakeAmount);

                _goEar1.transform.localRotation = Quaternion.identity;
                _goEar1.transform.Rotate(0.0f, 0.0f, -CRoleManager.s_Instance.m_fEarShakeAmount);
            }
            else
            {
                _goEar0.transform.localRotation = Quaternion.identity;
                _goEar0.transform.Rotate(0.0f, 0.0f, 0);

                _goEar1.transform.localRotation = Quaternion.identity;
                _goEar1.transform.Rotate(0.0f, 0.0f, 0);
            }

            m_fEarActionTimeElapse += Time.fixedDeltaTime;
            if (m_fEarActionTimeElapse >= CRoleManager.s_Instance.m_fEarShakeInterval)
            {
                m_nEarActionStatus++;
                m_fEarActionTimeElapse = 0;
            }
        }// end  if (m_nEarActionStatus > 0)              if ( m_nEarActionStatus > 4 )             {                 m_nEarActionStatus = 0;             }         }          


} // end class
